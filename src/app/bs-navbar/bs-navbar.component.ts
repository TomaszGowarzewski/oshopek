import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../Services/auth.service';
import { AppUser } from '../models/AppUser';
import { auth } from 'firebase';

@Component({
  selector: 'bs-navbar',
  templateUrl: './bs-navbar.component.html',
  styleUrls: ['./bs-navbar.component.css']
})
export class BsNavbarComponent  {
  appUser :AppUser;

  constructor(private authService:AuthService) {
    authService.appUser$.subscribe(appUser => this.appUser = appUser);
  }
  logout(){
  this.authService.logout();
  }
}