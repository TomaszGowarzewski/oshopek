import { Injectable } from '@angular/core';
import { CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { UserService } from './user.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class AdminAuthGuard implements CanActivate {

  constructor(private auth: AuthService,private router : Router,private userService : UserService) { }

  canActivate() : Observable<boolean> {
    return  this.auth.appUser$.map(appuser => appuser.isAdmin);
  }
}
